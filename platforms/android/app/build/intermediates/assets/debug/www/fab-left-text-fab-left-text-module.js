(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["fab-left-text-fab-left-text-module"],{

/***/ "./src/app/fab-left-text/fab-left-text.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/fab-left-text/fab-left-text.module.ts ***!
  \*******************************************************/
/*! exports provided: FabLeftTextPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FabLeftTextPageModule", function() { return FabLeftTextPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _fab_left_text_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./fab-left-text.page */ "./src/app/fab-left-text/fab-left-text.page.ts");







var routes = [
    {
        path: '',
        component: _fab_left_text_page__WEBPACK_IMPORTED_MODULE_6__["FabLeftTextPage"]
    }
];
var FabLeftTextPageModule = /** @class */ (function () {
    function FabLeftTextPageModule() {
    }
    FabLeftTextPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_fab_left_text_page__WEBPACK_IMPORTED_MODULE_6__["FabLeftTextPage"]]
        })
    ], FabLeftTextPageModule);
    return FabLeftTextPageModule;
}());



/***/ }),

/***/ "./src/app/fab-left-text/fab-left-text.page.html":
/*!*******************************************************!*\
  !*** ./src/app/fab-left-text/fab-left-text.page.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar>\n        <ion-buttons slot=\"start\">\n            <ion-button expand=\"block\" routerLink=\"/component-details\" routerDirection=\"backward\">\n                <ion-icon name=\"arrow-back\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n        <ion-title>Fab Left With Text</ion-title>\n    </ion-toolbar>\n</ion-header>\n<ion-content padding>\n    <ion-grid *ngFor=\"let item of items\">\n        <ion-text>\n            <h2>{{item.heading}}</h2>\n        </ion-text>\n\n        <ion-img src=\"{{item.image}}\"></ion-img>\n        <ion-row align-items-center>\n            <ion-col size=\"8\">\n                <ion-text>\n                    <h4>{{item.subheading}}</h4>\n                </ion-text>\n            </ion-col>\n            <ion-col align-self-end size=\"4\">\n                <ion-button>Share</ion-button>\n            </ion-col>\n        </ion-row>\n        <p>\n            {{item.para}}\n        </p>\n    </ion-grid>\n</ion-content>\n<ion-footer>\n    <!-- float action button placed at the end and bottom of screen -->\n    <ion-fab vertical=\"bottom\" horizontal=\"end\">\n        <ion-fab-button>Share</ion-fab-button>\n        <!-- fab list show more buttons from left side on click -->\n        <ion-fab-list side=\"start\">\n            <ion-fab-button>ABC</ion-fab-button>\n            <ion-fab-button>ABC</ion-fab-button>\n            <ion-fab-button>ABC</ion-fab-button>\n            <ion-fab-button>ABC</ion-fab-button>\n            <ion-fab-button>ABC</ion-fab-button>\n        </ion-fab-list>\n    </ion-fab>\n</ion-footer>"

/***/ }),

/***/ "./src/app/fab-left-text/fab-left-text.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/fab-left-text/fab-left-text.page.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content ion-grid {\n  padding: 0 0 16px 0;\n  margin-bottom: 12px;\n  border-bottom: 1px solid #ddd; }\n  ion-content ion-grid ion-text h2 {\n    margin-top: 0; }\n  ion-content ion-grid ion-text h4 {\n    margin: 0; }\n  ion-content ion-grid ion-button {\n    margin-left: 0;\n    margin-right: 0; }\n  ion-content ion-grid:last-child {\n    border-bottom: none;\n    margin-bottom: 0;\n    padding-bottom: 0; }\n  ion-content ion-grid ion-row ion-col:first-child {\n    padding-left: 0; }\n  ion-content ion-grid ion-row ion-col:last-child {\n    text-align: right;\n    padding-right: 0; }\n  ion-footer ion-fab {\n  position: fixed; }\n  ion-footer ion-fab ion-icon {\n    zoom: 1; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZmFiLWxlZnQtdGV4dC9DOlxceGFtcHBcXGh0ZG9jc1xcaW9uaWMtNC11aS0xL3NyY1xcYXBwXFxmYWItbGVmdC10ZXh0XFxmYWItbGVmdC10ZXh0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUlJLG1CQUFtQjtFQUNuQixtQkFBa0I7RUFDbEIsNkJBQTRCLEVBQUE7RUFOaEM7SUFVTSxhQUFZLEVBQUE7RUFWbEI7SUFhTSxTQUFRLEVBQUE7RUFiZDtJQWtCSSxjQUFhO0lBQ2IsZUFBYyxFQUFBO0VBbkJsQjtJQXVCSSxtQkFBbUI7SUFDbkIsZ0JBQWdCO0lBQ2hCLGlCQUFpQixFQUFBO0VBekJyQjtJQWlDTSxlQUFjLEVBQUE7RUFqQ3BCO0lBb0NRLGlCQUFpQjtJQUNqQixnQkFBZSxFQUFBO0VBTXZCO0VBRUksZUFBZSxFQUFBO0VBRm5CO0lBSU0sT0FBTyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvZmFiLWxlZnQtdGV4dC9mYWItbGVmdC10ZXh0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vb3ZlcmFsbCBjb250ZW50IG9mIHBhZ2VcclxuaW9uLWNvbnRlbnR7XHJcbiAgLy9ncmlkIGluc2lkZSBjb250ZW50IFxyXG4gIGlvbi1ncmlke1xyXG4gICAgLy9hcHBsaWVkIG9uIG92ZXJhbGwgZ3JpZCBcclxuICAgIHBhZGRpbmc6IDAgMCAxNnB4IDA7XHJcbiAgICBtYXJnaW4tYm90dG9tOjEycHg7XHJcbiAgICBib3JkZXItYm90dG9tOjFweCBzb2xpZCAjZGRkO1xyXG4gICAgLy90ZXh0IGNvbnRhaW5pbmcgaDIgYW5kIGg0IHRhZyBcclxuICAgIGlvbi10ZXh0e1xyXG4gICAgICBoMntcclxuICAgICAgbWFyZ2luLXRvcDowO1xyXG4gICAgICB9XHJcbiAgICAgIGg0e1xyXG4gICAgICBtYXJnaW46MDtcclxuICAgIH1cclxuICB9XHJcbiAgLy9hbGwgYnV0dG9uc1xyXG4gIGlvbi1idXR0b257XHJcbiAgICBtYXJnaW4tbGVmdDowO1xyXG4gICAgbWFyZ2luLXJpZ2h0OjA7XHJcbiAgfVxyXG4gIC8vbGFzdCBidXR0b24gXHJcbiAgJjpsYXN0LWNoaWxke1xyXG4gICAgYm9yZGVyLWJvdHRvbTogbm9uZTtcclxuICAgIG1hcmdpbi1ib3R0b206IDA7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMDtcclxuICB9XHJcbiAgLy9vdmVyYWxsIHJvdyBcclxuICBpb24tcm93e1xyXG4gICAgLy9vdmVyYWxsIGNvbHVtbiBpbnNpZGUgcm93IFxyXG4gICAgaW9uLWNvbHtcclxuICAgICAgLy9maXJzdCBjb2x1bW4gb2Ygcm93XHJcbiAgICAgICY6Zmlyc3QtY2hpbGR7XHJcbiAgICAgIHBhZGRpbmctbGVmdDowO1xyXG4gICAgICB9XHJcbiAgICAgICY6bGFzdC1jaGlsZHtcclxuICAgICAgICB0ZXh0LWFsaWduOiByaWdodDtcclxuICAgICAgICBwYWRkaW5nLXJpZ2h0OjA7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbmlvbi1mb290ZXJ7XHJcbiAgaW9uLWZhYntcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIGlvbi1pY29ue1xyXG4gICAgICB6b29tOiAxO1xyXG4gICAgfVxyXG4gIH0gIFxyXG59Il19 */"

/***/ }),

/***/ "./src/app/fab-left-text/fab-left-text.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/fab-left-text/fab-left-text.page.ts ***!
  \*****************************************************/
/*! exports provided: FabLeftTextPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FabLeftTextPage", function() { return FabLeftTextPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FabLeftTextPage = /** @class */ (function () {
    function FabLeftTextPage() {
        this.items = [
            { heading: "News One", image: "assets/images/Pictures/gallery-images/gallery_image2.png", subheading: "Label Text", para: "Text buttons and contained buttons use text labels, which describe the action that will occur if a user taps a button. If a text label is not used, an icon should be present to signify what the button does. By default Material uses capitalizedbutton text labels (for languages that have capitalization). This is to distinguish the text label from surrounding text. If a text button does not use capitalization for button text, find another characteristic to distinguish it such as color,size, or placement." },
            { heading: "News Two", image: "assets/images/Pictures/gallery-images/gallery_image3.png", subheading: "Label Text", para: "Text buttons and contained buttons use text labels, which describe the action that will occur if a user taps a button. If a text label is not used, an icon should be present to signify what the button does. By default Material uses capitalizedbutton text labels (for languages that have capitalization). This is to distinguish the text label from surrounding text. If a text button does not use capitalization for button text, find another characteristic to distinguish it such as color,size, or placement." },
            { heading: "News Three", image: "assets/images/Pictures/gallery-images/gallery_image4.png", subheading: "Label Text", para: "Text buttons and contained buttons use text labels, which describe the action that will occur if a user taps a button. If a text label is not used, an icon should be present to signify what the button does. By default Material uses capitalizedbutton text labels (for languages that have capitalization). This is to distinguish the text label from surrounding text. If a text button does not use capitalization for button text, find another characteristic to distinguish it such as color,size, or placement." },
        ];
    }
    FabLeftTextPage.prototype.ngOnInit = function () {
    };
    FabLeftTextPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-fab-left-text',
            template: __webpack_require__(/*! ./fab-left-text.page.html */ "./src/app/fab-left-text/fab-left-text.page.html"),
            styles: [__webpack_require__(/*! ./fab-left-text.page.scss */ "./src/app/fab-left-text/fab-left-text.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], FabLeftTextPage);
    return FabLeftTextPage;
}());



/***/ })

}]);
//# sourceMappingURL=fab-left-text-fab-left-text-module.js.map