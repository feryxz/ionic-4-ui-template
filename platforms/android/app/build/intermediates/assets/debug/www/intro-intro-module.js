(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["intro-intro-module"],{

/***/ "./src/app/intro/intro.module.ts":
/*!***************************************!*\
  !*** ./src/app/intro/intro.module.ts ***!
  \***************************************/
/*! exports provided: IntroPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IntroPageModule", function() { return IntroPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _intro_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./intro.page */ "./src/app/intro/intro.page.ts");







var routes = [
    {
        path: '',
        component: _intro_page__WEBPACK_IMPORTED_MODULE_6__["IntroPage"]
    }
];
var IntroPageModule = /** @class */ (function () {
    function IntroPageModule() {
    }
    IntroPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_intro_page__WEBPACK_IMPORTED_MODULE_6__["IntroPage"]]
        })
    ], IntroPageModule);
    return IntroPageModule;
}());



/***/ }),

/***/ "./src/app/intro/intro.page.html":
/*!***************************************!*\
  !*** ./src/app/intro/intro.page.html ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content fullscreen=\"true\" no-padding>\n  <ion-slides pager=\"true\" #IonSlides loop=\"true\" no-padding (ionSlideDidChange)=\"slideChanged()\">\n    <ion-slide #IonSlides *ngFor=\"let slide of slides; let i=index\" no-padding>\n      <span no-padding>\n        <ion-row justify-content-center>\n          <img src=\"{{slide.image}}\" alt=\"Slide Images\">\n        </ion-row>\n        <ion-row class=\"icon-row\" justify-content-center>\n          <ion-icon name=\"{{slide.icon}}\"></ion-icon>\n        </ion-row>\n        <ion-row justify-content-center>\n          <h2 class=\"slideTitle\">{{slide.title}}</h2>\n        </ion-row>\n        <ion-row justify-content-center>\n          <ion-col>\n            <p>{{slide.text}}</p>\n          </ion-col>\n        </ion-row>\n      </span>\n    </ion-slide>\n  </ion-slides>\n  <ion-row>\n    <ion-col size=\"2\">\n      <div>\n        <ion-button *ngIf=\"!visiable\" routerLink=\"/home\" routerDirection=\"forward\" fill=\"clear\">Skip</ion-button>\n      </div>\n    </ion-col>\n    <ion-col size=\"8\">\n    </ion-col>\n    <ion-col size=\"2\">\n      <ion-icon *ngIf=\"!visiable\" (click)=\"nextSlide()\" name=\"arrow-round-forward\"></ion-icon>\n      <ion-button *ngIf=\"visiable\" routerLink=\"/home\" routerDirection=\"forward\" fill=\"clear\">Done</ion-button>\n    </ion-col>\n  </ion-row>\n</ion-content>"

/***/ }),

/***/ "./src/app/intro/intro.page.scss":
/*!***************************************!*\
  !*** ./src/app/intro/intro.page.scss ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --background:none !important;\n  background: url('introbg.png');\n  background-position: top center;\n  background-size: auto 100%;\n  background-repeat: no-repeat; }\n  ion-content ion-slides {\n    height: 99%;\n    --bullet-background:black !important;\n    --bullet-background-active:red !important; }\n  ion-content ion-slides span {\n      margin-top: -10px;\n      margin-left: auto;\n      margin-right: auto;\n      width: 100%; }\n  ion-content ion-slides span ion-row {\n        border-top: none !important;\n        margin-top: 2%; }\n  ion-content ion-slides span ion-row:first-child {\n          width: auto;\n          height: 350px; }\n  ion-content ion-row {\n    margin-top: -40px;\n    border-top: solid gray;\n    border-width: 0.5px; }\n  ion-content ion-row ion-col:first-child ion-button {\n      color: red;\n      --border-color:red;\n      height: 30px;\n      margin-top: 4px;\n      position: absolute;\n      z-index: 999; }\n  ion-content ion-row ion-col:last-child ion-icon {\n      color: red;\n      zoom: 1.3;\n      margin-top: -5px;\n      padding-top: 10px;\n      position: absolute;\n      z-index: 999; }\n  ion-content ion-row ion-col:last-child ion-button {\n      color: red;\n      --border-color: red;\n      height: 30px;\n      margin-top: 2px;\n      position: absolute;\n      z-index: 999;\n      margin-left: -30px; }\n  p {\n  text-align: center;\n  color: gray; }\n  ion-item {\n  --background:white !important;\n  width: 100%; }\n  ion-row {\n  margin-top: 2%; }\n  .icon-row {\n  margin-top: 1%; }\n  .icon-row ion-icon {\n    height: 2em;\n    width: 2em;\n    color: red; }\n  .slideTitle {\n  font-size: 18px;\n  margin-top: 5px;\n  margin-bottom: 0px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaW50cm8vQzpcXHhhbXBwXFxodGRvY3NcXGlvbmljLTQtdWktMS9zcmNcXGFwcFxcaW50cm9cXGludHJvLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLDRCQUFhO0VBQ2IsOEJBQWlEO0VBQ2pELCtCQUErQjtFQUMvQiwwQkFBeUI7RUFDekIsNEJBQTRCLEVBQUE7RUFMOUI7SUFPSSxXQUFXO0lBQ1gsb0NBQW9CO0lBQ3BCLHlDQUEyQixFQUFBO0VBVC9CO01BV00saUJBQWlCO01BQ2pCLGlCQUFpQjtNQUNqQixrQkFBa0I7TUFDbEIsV0FBVyxFQUFBO0VBZGpCO1FBZ0JRLDJCQUEyQjtRQUMzQixjQUFjLEVBQUE7RUFqQnRCO1VBbUJVLFdBQVc7VUFDWCxhQUFhLEVBQUE7RUFwQnZCO0lBMEJJLGlCQUFpQjtJQUNqQixzQkFBc0I7SUFDdEIsbUJBQW1CLEVBQUE7RUE1QnZCO01BZ0NVLFVBQVU7TUFDVixrQkFBZTtNQUNmLFlBQVk7TUFDWixlQUFlO01BQ2Ysa0JBQWtCO01BQ2xCLFlBQVksRUFBQTtFQXJDdEI7TUEwQ1EsVUFBVTtNQUNWLFNBQVM7TUFDVCxnQkFBZ0I7TUFDaEIsaUJBQWlCO01BQ2pCLGtCQUFrQjtNQUNsQixZQUFZLEVBQUE7RUEvQ3BCO01Ba0RRLFVBQVU7TUFDVixtQkFBZTtNQUNmLFlBQVk7TUFDWixlQUFlO01BQ2Ysa0JBQWtCO01BQ2xCLFlBQVk7TUFDWixrQkFBa0IsRUFBQTtFQU0xQjtFQUNFLGtCQUFrQjtFQUNsQixXQUFXLEVBQUE7RUFFYjtFQUNFLDZCQUFhO0VBQ2IsV0FBVyxFQUFBO0VBRWI7RUFDRSxjQUFjLEVBQUE7RUFHaEI7RUFDRSxjQUFjLEVBQUE7RUFEaEI7SUFHRSxXQUFXO0lBQ1gsVUFBVTtJQUNWLFVBQ0EsRUFBQTtFQUdGO0VBQ0UsZUFBZTtFQUNmLGVBQWU7RUFDZixrQkFBa0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2ludHJvL2ludHJvLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50e1xyXG4gIC0tYmFja2dyb3VuZDpub25lICFpbXBvcnRhbnQ7XHJcbiAgYmFja2dyb3VuZDp1cmwoJy4uLy4uL2Fzc2V0cy9pbWFnZXMvaW50cm9iZy5wbmcnKTtcclxuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiB0b3AgY2VudGVyO1xyXG4gIGJhY2tncm91bmQtc2l6ZTphdXRvIDEwMCU7XHJcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICBpb24tc2xpZGVze1xyXG4gICAgaGVpZ2h0OiA5OSU7XHJcbiAgICAtLWJ1bGxldC1iYWNrZ3JvdW5kOmJsYWNrICFpbXBvcnRhbnQ7XHJcbiAgICAtLWJ1bGxldC1iYWNrZ3JvdW5kLWFjdGl2ZTpyZWQgIWltcG9ydGFudDtcclxuICAgIHNwYW57XHJcbiAgICAgIG1hcmdpbi10b3A6IC0xMHB4O1xyXG4gICAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgaW9uLXJvd3tcclxuICAgICAgICBib3JkZXItdG9wOiBub25lICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMiU7XHJcbiAgICAgICAgJjpmaXJzdC1jaGlsZHtcclxuICAgICAgICAgIHdpZHRoOiBhdXRvO1xyXG4gICAgICAgICAgaGVpZ2h0OiAzNTBweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgaW9uLXJvd3tcclxuICAgIG1hcmdpbi10b3A6IC00MHB4O1xyXG4gICAgYm9yZGVyLXRvcDogc29saWQgZ3JheTtcclxuICAgIGJvcmRlci13aWR0aDogMC41cHg7XHJcbiAgICBpb24tY29se1xyXG4gICAgICAmOmZpcnN0LWNoaWxke1xyXG4gICAgICAgIGlvbi1idXR0b257XHJcbiAgICAgICAgICBjb2xvcjogcmVkO1xyXG4gICAgICAgICAgLS1ib3JkZXItY29sb3I6cmVkO1xyXG4gICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgbWFyZ2luLXRvcDogNHB4O1xyXG4gICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgei1pbmRleDogOTk5O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAmOmxhc3QtY2hpbGR7XHJcbiAgICAgIGlvbi1pY29ue1xyXG4gICAgICAgIGNvbG9yOiByZWQ7XHJcbiAgICAgICAgem9vbTogMS4zO1xyXG4gICAgICAgIG1hcmdpbi10b3A6IC01cHg7XHJcbiAgICAgICAgcGFkZGluZy10b3A6IDEwcHg7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIHotaW5kZXg6IDk5OTtcclxuICAgICAgICB9XHJcbiAgICAgIGlvbi1idXR0b257XHJcbiAgICAgICAgY29sb3I6IHJlZDtcclxuICAgICAgICAtLWJvcmRlci1jb2xvcjogcmVkO1xyXG4gICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgICBtYXJnaW4tdG9wOiAycHg7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIHotaW5kZXg6IDk5OTtcclxuICAgICAgICBtYXJnaW4tbGVmdDogLTMwcHg7ICBcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH0gIFxyXG4gIH1cclxufVxyXG5we1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBjb2xvcjogZ3JheTtcclxufVxyXG5pb24taXRlbXtcclxuICAtLWJhY2tncm91bmQ6d2hpdGUgIWltcG9ydGFudDtcclxuICB3aWR0aDogMTAwJTsgXHJcbn1cclxuaW9uLXJvd3tcclxuICBtYXJnaW4tdG9wOiAyJTsgICAgICBcclxufVxyXG4vLzJuZCByb3dcclxuLmljb24tcm93e1xyXG4gIG1hcmdpbi10b3A6IDElO1xyXG4gIGlvbi1pY29ue1xyXG4gIGhlaWdodDogMmVtO1xyXG4gIHdpZHRoOiAyZW07XHJcbiAgY29sb3I6IHJlZFxyXG4gIH1cclxufVxyXG4vLzNyZCByb3dcclxuLnNsaWRlVGl0bGV7XHJcbiAgZm9udC1zaXplOiAxOHB4O1xyXG4gIG1hcmdpbi10b3A6IDVweDtcclxuICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/intro/intro.page.ts":
/*!*************************************!*\
  !*** ./src/app/intro/intro.page.ts ***!
  \*************************************/
/*! exports provided: IntroPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IntroPage", function() { return IntroPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/splash-screen/ngx */ "./node_modules/@ionic-native/splash-screen/ngx/index.js");




var IntroPage = /** @class */ (function () {
    function IntroPage(splashScreen) {
        this.splashScreen = splashScreen;
        this.visiable = false;
        this.slides = [
            { image: "assets/images/intro/slides_1.gif", title: "Home Page", icon: "home", text: "Home screen contain all themes at top. You can select component screen, UI screens and many more from home page. " },
            { image: "assets/images/intro/slides_2.gif", title: "Component Details Page", icon: "apps", text: "Cmponent details page contain all 90+ screens of ionic components. You can use them instead of creating them from scratch" },
            { image: "assets/images/intro/slides_3.gif", title: "UI screens", icon: "browsers", text: "Comming Soon" },
        ];
    }
    IntroPage.prototype.nextSlide = function () {
        this.autoSlides.slideNext();
    };
    IntroPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        setTimeout(function () {
            _this.splashScreen.hide();
        }, 1500);
        this.autoSlides.startAutoplay();
    };
    IntroPage.prototype.ngOnInit = function () { };
    IntroPage.prototype.slideChanged = function () {
        var _this = this;
        this.autoSlides.getActiveIndex().then(function (index) {
            console.log(index);
            if (index == 2) {
                _this.visiable = true;
            }
            else {
                _this.visiable = false;
            }
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonSlides"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonSlides"])
    ], IntroPage.prototype, "autoSlides", void 0);
    IntroPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-intro',
            template: __webpack_require__(/*! ./intro.page.html */ "./src/app/intro/intro.page.html"),
            styles: [__webpack_require__(/*! ./intro.page.scss */ "./src/app/intro/intro.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__["SplashScreen"]])
    ], IntroPage);
    return IntroPage;
}());



/***/ })

}]);
//# sourceMappingURL=intro-intro-module.js.map