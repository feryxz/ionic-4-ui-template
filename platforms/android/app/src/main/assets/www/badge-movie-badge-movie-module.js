(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["badge-movie-badge-movie-module"],{

/***/ "./src/app/badge-movie/badge-movie.module.ts":
/*!***************************************************!*\
  !*** ./src/app/badge-movie/badge-movie.module.ts ***!
  \***************************************************/
/*! exports provided: BadgeMoviePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BadgeMoviePageModule", function() { return BadgeMoviePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _badge_movie_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./badge-movie.page */ "./src/app/badge-movie/badge-movie.page.ts");







var routes = [
    {
        path: '',
        component: _badge_movie_page__WEBPACK_IMPORTED_MODULE_6__["BadgeMoviePage"]
    }
];
var BadgeMoviePageModule = /** @class */ (function () {
    function BadgeMoviePageModule() {
    }
    BadgeMoviePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_badge_movie_page__WEBPACK_IMPORTED_MODULE_6__["BadgeMoviePage"]]
        })
    ], BadgeMoviePageModule);
    return BadgeMoviePageModule;
}());



/***/ }),

/***/ "./src/app/badge-movie/badge-movie.page.html":
/*!***************************************************!*\
  !*** ./src/app/badge-movie/badge-movie.page.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content no-padding >\n      <ion-card *ngFor=\"let p of items\">\n          <img  src=\"{{p.img}}\" />\n          <ion-card-header>\n            <ion-card-title>Lorem Ipsum Title</ion-card-title>\n            <ion-card-subtitle>Lorem Ipsum Subtitle</ion-card-subtitle>\n          </ion-card-header>\n      </ion-card>\n</ion-content>"

/***/ }),

/***/ "./src/app/badge-movie/badge-movie.page.scss":
/*!***************************************************!*\
  !*** ./src/app/badge-movie/badge-movie.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content ion-card ion-card-header {\n  position: absolute;\n  top: 50%;\n  left: 0;\n  width: 100%;\n  -webkit-transform: translateY(-50%);\n          transform: translateY(-50%);\n  text-align: center; }\n  ion-content ion-card ion-card-header ion-card-title, ion-content ion-card ion-card-header ion-card-subtitle {\n    font-weight: bold;\n    color: white; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYmFkZ2UtbW92aWUvQzpcXHhhbXBwXFxodGRvY3NcXGlvbmljLTQtdWktMS9zcmNcXGFwcFxcYmFkZ2UtbW92aWVcXGJhZGdlLW1vdmllLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUdNLGtCQUFrQjtFQUNsQixRQUFRO0VBQ1IsT0FBTztFQUNQLFdBQVc7RUFDWCxtQ0FBMkI7VUFBM0IsMkJBQTJCO0VBQzNCLGtCQUFrQixFQUFBO0VBUnhCO0lBVVEsaUJBQWlCO0lBQ2pCLFlBQVcsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2JhZGdlLW1vdmllL2JhZGdlLW1vdmllLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50e1xyXG4gIGlvbi1jYXJke1xyXG4gICAgaW9uLWNhcmQtaGVhZGVye1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIHRvcDogNTAlO1xyXG4gICAgICBsZWZ0OiAwO1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xyXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgIGlvbi1jYXJkLXRpdGxlLCBpb24tY2FyZC1zdWJ0aXRsZXtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBjb2xvcjp3aGl0ZTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/badge-movie/badge-movie.page.ts":
/*!*************************************************!*\
  !*** ./src/app/badge-movie/badge-movie.page.ts ***!
  \*************************************************/
/*! exports provided: BadgeMoviePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BadgeMoviePage", function() { return BadgeMoviePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var BadgeMoviePage = /** @class */ (function () {
    function BadgeMoviePage() {
        this.items = [
            { text: "Lorem Ipsum", img: 'assets/images/Pictures/gallery-images/gallery_image1.png' },
            { text: "Lorem Ipsum", img: 'assets/images/Pictures/gallery-images/gallery_image2.png' },
            { text: "Lorem Ipsum", img: 'assets/images/Pictures/gallery-images/gallery_image3.png' },
            { text: "Lorem Ipsum", img: 'assets/images/Pictures/gallery-images/gallery_image4.png' },
            { text: "Lorem Ipsum", img: 'assets/images/Pictures/gallery-images/gallery_image5.png' },
            { text: "Lorem Ipsum", img: 'assets/images/Pictures/gallery-images/gallery_image6.png' },
            { text: "Lorem Ipsum", img: 'assets/images/Pictures/gallery-images/gallery_image7.png' },
        ];
    }
    BadgeMoviePage.prototype.ngOnInit = function () {
    };
    BadgeMoviePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-badge-movie',
            template: __webpack_require__(/*! ./badge-movie.page.html */ "./src/app/badge-movie/badge-movie.page.html"),
            styles: [__webpack_require__(/*! ./badge-movie.page.scss */ "./src/app/badge-movie/badge-movie.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], BadgeMoviePage);
    return BadgeMoviePage;
}());



/***/ })

}]);
//# sourceMappingURL=badge-movie-badge-movie-module.js.map