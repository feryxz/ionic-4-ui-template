(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["card-timeline-theme-one-card-timeline-theme-one-module"],{

/***/ "./src/app/card-timeline-theme-one/card-timeline-theme-one.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/card-timeline-theme-one/card-timeline-theme-one.module.ts ***!
  \***************************************************************************/
/*! exports provided: CardTimelineThemeOnePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardTimelineThemeOnePageModule", function() { return CardTimelineThemeOnePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _card_timeline_theme_one_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./card-timeline-theme-one.page */ "./src/app/card-timeline-theme-one/card-timeline-theme-one.page.ts");







var routes = [
    {
        path: '',
        component: _card_timeline_theme_one_page__WEBPACK_IMPORTED_MODULE_6__["CardTimelineThemeOnePage"]
    }
];
var CardTimelineThemeOnePageModule = /** @class */ (function () {
    function CardTimelineThemeOnePageModule() {
    }
    CardTimelineThemeOnePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_card_timeline_theme_one_page__WEBPACK_IMPORTED_MODULE_6__["CardTimelineThemeOnePage"]]
        })
    ], CardTimelineThemeOnePageModule);
    return CardTimelineThemeOnePageModule;
}());



/***/ }),

/***/ "./src/app/card-timeline-theme-one/card-timeline-theme-one.page.html":
/*!***************************************************************************!*\
  !*** ./src/app/card-timeline-theme-one/card-timeline-theme-one.page.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar>\n        <ion-buttons slot=\"start\">\n            <ion-button expand=\"block\" routerLink=\"/component-details\" routerDirection=\"backward\">\n                <ion-icon name=\"arrow-back\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n        <ion-title>Cards Timeline Theme 1</ion-title>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content no-padding>\n    <!-- card with content and item with person profile  -->\n    <ion-card *ngFor=\"let item of items\">\n        <ion-item>\n            <ion-avatar slot=\"start\">\n                <img src=\"{{item.img}}\" />\n            </ion-avatar>\n            <ion-label>\n                <h3>{{item.text}}</h3>\n                <p>{{item.dob}}</p>\n            </ion-label>\n        </ion-item>\n        <ion-img src=\"{{item.img}}\"></ion-img>\n        <ion-card-content>\n            <p>Wait a minute. Wait a minute, Doc. Uhhh... Are you telling me that you built a time machine... out of a DeLorean?!\n                Whoa. This is heavy.</p>\n        </ion-card-content>\n        <ion-row>\n            <ion-col size=\"4\" no-padding>\n                <ion-button size=\"small\" color=\"primary\" fill=\"clear\">\n                    <ion-icon name='thumbs-up'></ion-icon>\n                    12 Likes\n                </ion-button>\n            </ion-col>\n            <ion-col size=\"5\" no-padding>\n                <ion-button color=\"primary\" size=\"small\" fill=\"clear\">\n                    <ion-icon name='text'></ion-icon>\n                    4 Comments\n                </ion-button>\n            </ion-col>\n            <ion-col size=\"3\" no-padding>\n                <ion-button size=\"small\" fill=\"clear\">\n                    11h ago\n                </ion-button>\n            </ion-col>\n        </ion-row>\n    </ion-card>\n</ion-content>"

/***/ }),

/***/ "./src/app/card-timeline-theme-one/card-timeline-theme-one.page.scss":
/*!***************************************************************************!*\
  !*** ./src/app/card-timeline-theme-one/card-timeline-theme-one.page.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "aion-content ion-card {\n  margin: 0; }\n  aion-content ion-card ion-row ion-col {\n    text-align: start; }\n  aion-content ion-card ion-row ion-col:last-child {\n    text-align: center; }\n  aion-content ion-card ion-row ion-col:first-child {\n    text-align: end; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2FyZC10aW1lbGluZS10aGVtZS1vbmUvQzpcXHhhbXBwXFxodGRvY3NcXGlvbmljLTQtdWktMS9zcmNcXGFwcFxcY2FyZC10aW1lbGluZS10aGVtZS1vbmVcXGNhcmQtdGltZWxpbmUtdGhlbWUtb25lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVRLFNBQVEsRUFBQTtFQUZoQjtJQUtnQixpQkFBaUIsRUFBQTtFQUxqQztJQVFnQixrQkFBa0IsRUFBQTtFQVJsQztJQVdnQixlQUFlLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9jYXJkLXRpbWVsaW5lLXRoZW1lLW9uZS9jYXJkLXRpbWVsaW5lLXRoZW1lLW9uZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJhaW9uLWNvbnRlbnR7XG4gICAgaW9uLWNhcmR7XG4gICAgICAgIG1hcmdpbjowO1xuICAgICAgICBpb24tcm93e1xuICAgICAgICAgICAgaW9uLWNvbHtcbiAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBzdGFydDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlvbi1jb2w6bGFzdC1jaGlsZHtcbiAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpb24tY29sOmZpcnN0LWNoaWxke1xuICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGVuZDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/card-timeline-theme-one/card-timeline-theme-one.page.ts":
/*!*************************************************************************!*\
  !*** ./src/app/card-timeline-theme-one/card-timeline-theme-one.page.ts ***!
  \*************************************************************************/
/*! exports provided: CardTimelineThemeOnePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardTimelineThemeOnePage", function() { return CardTimelineThemeOnePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var CardTimelineThemeOnePage = /** @class */ (function () {
    function CardTimelineThemeOnePage() {
        this.items = [
            { text: "Maria James", img: 'assets/images/Pictures/gallery-images/gallery_image1.png', dob: "November 5, 2015" },
            { text: "Jeans Stress", img: 'assets/images/Pictures/gallery-images/gallery_image2.png', dob: "November 5, 2015" },
            { text: "Emma Christian", img: 'assets/images/Pictures/gallery-images/gallery_image3.png', dob: "November 5, 2015" },
            { text: "Julia Jan", img: 'assets/images/Pictures/gallery-images/gallery_image4.png', dob: "November 5, 2015" },
        ];
    }
    CardTimelineThemeOnePage.prototype.ngOnInit = function () {
    };
    CardTimelineThemeOnePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-card-timeline-theme-one',
            template: __webpack_require__(/*! ./card-timeline-theme-one.page.html */ "./src/app/card-timeline-theme-one/card-timeline-theme-one.page.html"),
            styles: [__webpack_require__(/*! ./card-timeline-theme-one.page.scss */ "./src/app/card-timeline-theme-one/card-timeline-theme-one.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CardTimelineThemeOnePage);
    return CardTimelineThemeOnePage;
}());



/***/ })

}]);
//# sourceMappingURL=card-timeline-theme-one-card-timeline-theme-one-module.js.map