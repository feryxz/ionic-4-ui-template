(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["actionsheet-simple-actionsheet-simple-module"],{

/***/ "./src/app/actionsheet-simple/actionsheet-simple.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/actionsheet-simple/actionsheet-simple.module.ts ***!
  \*****************************************************************/
/*! exports provided: ActionsheetSimplePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActionsheetSimplePageModule", function() { return ActionsheetSimplePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _actionsheet_simple_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./actionsheet-simple.page */ "./src/app/actionsheet-simple/actionsheet-simple.page.ts");







var routes = [
    {
        path: '',
        component: _actionsheet_simple_page__WEBPACK_IMPORTED_MODULE_6__["ActionsheetSimplePage"]
    }
];
var ActionsheetSimplePageModule = /** @class */ (function () {
    function ActionsheetSimplePageModule() {
    }
    ActionsheetSimplePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_actionsheet_simple_page__WEBPACK_IMPORTED_MODULE_6__["ActionsheetSimplePage"]]
        })
    ], ActionsheetSimplePageModule);
    return ActionsheetSimplePageModule;
}());



/***/ }),

/***/ "./src/app/actionsheet-simple/actionsheet-simple.page.html":
/*!*****************************************************************!*\
  !*** ./src/app/actionsheet-simple/actionsheet-simple.page.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-button expand=\"block\" routerLink=\"/component-details\" routerDirection=\"backward\">\n        <ion-icon name=\"arrow-back\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n    <ion-title>Action Sheet Simple</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-grid *ngFor=\"let item of items\">\n    <ion-text>\n      <h2>{{item.heading}}</h2>\n    </ion-text>\n    <ion-img src=\"{{item.image}}\"></ion-img>\n    <!-- presentActionSheet() get the property of action sheet which you will get from actionsheet-simple.ts file -->\n    <ion-button expand=\"full\" class=\"view\" (click)=\"presentActionSheet()\">Share</ion-button>\n    <ion-text>\n      <h4>{{item.subheading}}</h4>\n    </ion-text>\n    <p>\n      {{item.para}}\n    </p>\n  </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./src/app/actionsheet-simple/actionsheet-simple.page.scss":
/*!*****************************************************************!*\
  !*** ./src/app/actionsheet-simple/actionsheet-simple.page.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content ion-grid {\n  padding: 0 0 16px 0;\n  margin-bottom: 12px;\n  border-bottom: 1px solid #ddd; }\n  ion-content ion-grid ion-text h2 {\n    margin-top: 0; }\n  ion-content ion-grid ion-img {\n    margin-bottom: 16px; }\n  ion-content ion-grid ion-button {\n    margin-left: 0;\n    margin-right: 0;\n    margin-bottom: 16px; }\n  ion-content ion-grid:last-child {\n    border-bottom: none;\n    margin-bottom: 0;\n    padding-bottom: 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWN0aW9uc2hlZXQtc2ltcGxlL0M6XFx4YW1wcFxcaHRkb2NzXFxpb25pYy00LXVpLTEvc3JjXFxhcHBcXGFjdGlvbnNoZWV0LXNpbXBsZVxcYWN0aW9uc2hlZXQtc2ltcGxlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVJLG1CQUFtQjtFQUNuQixtQkFBa0I7RUFDbEIsNkJBQTRCLEVBQUE7RUFKaEM7SUFPUSxhQUFZLEVBQUE7RUFQcEI7SUFXTSxtQkFBa0IsRUFBQTtFQVh4QjtJQWNNLGNBQWE7SUFDYixlQUFjO0lBQ2QsbUJBQW1CLEVBQUE7RUFoQnpCO0lBb0JNLG1CQUFtQjtJQUNuQixnQkFBZ0I7SUFDaEIsaUJBQWlCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9hY3Rpb25zaGVldC1zaW1wbGUvYWN0aW9uc2hlZXQtc2ltcGxlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50e1xyXG4gIGlvbi1ncmlke1xyXG4gICAgcGFkZGluZzogMCAwIDE2cHggMDtcclxuICAgIG1hcmdpbi1ib3R0b206MTJweDtcclxuICAgIGJvcmRlci1ib3R0b206MXB4IHNvbGlkICNkZGQ7IFxyXG4gICAgaW9uLXRleHR7XHJcbiAgICAgIGgye1xyXG4gICAgICAgIG1hcmdpbi10b3A6MDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgaW9uLWltZ3tcclxuICAgICAgbWFyZ2luLWJvdHRvbToxNnB4O1xyXG4gICAgfVxyXG4gICAgaW9uLWJ1dHRvbntcclxuICAgICAgbWFyZ2luLWxlZnQ6MDtcclxuICAgICAgbWFyZ2luLXJpZ2h0OjA7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDE2cHg7XHJcbiAgICB9XHJcblxyXG4gICAgJjpsYXN0LWNoaWxke1xyXG4gICAgICBib3JkZXItYm90dG9tOiBub25lO1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAwO1xyXG4gICAgICBwYWRkaW5nLWJvdHRvbTogMDtcclxuICAgIH1cclxuICB9XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/actionsheet-simple/actionsheet-simple.page.ts":
/*!***************************************************************!*\
  !*** ./src/app/actionsheet-simple/actionsheet-simple.page.ts ***!
  \***************************************************************/
/*! exports provided: ActionsheetSimplePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActionsheetSimplePage", function() { return ActionsheetSimplePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");


 //actionsheet controller package
var ActionsheetSimplePage = /** @class */ (function () {
    //action sheet package declaration
    function ActionsheetSimplePage(actionSheetController) {
        this.actionSheetController = actionSheetController;
        this.items = [
            { heading: "Article One", image: "assets/images/Pictures/gallery-images/gallery_image3.png", subheading: "Label Text", para: "Text buttons and contained buttons use text labels, which describe the action that will occur if a user taps a button. If a text label is not used, an icon should be present to signify what the button does. By default Material uses capitalizedbutton text labels (for languages that have capitalization). This is to distinguish the text label from surrounding text. If a text button does not use capitalization for button text, find another characteristic to distinguish it such as color,size, or placement." },
            { heading: "Article Two", image: "assets/images/Pictures/gallery-images/gallery_image4.png", subheading: "Label Text", para: "Text buttons and contained buttons use text labels, which describe the action that will occur if a user taps a button. If a text label is not used, an icon should be present to signify what the button does. By default Material uses capitalizedbutton text labels (for languages that have capitalization). This is to distinguish the text label from surrounding text. If a text button does not use capitalization for button text, find another characteristic to distinguish it such as color,size, or placement." },
            { heading: "Article Three", image: "assets/images/Pictures/gallery-images/gallery_image5.png", subheading: "Label Text", para: "Text buttons and contained buttons use text labels, which describe the action that will occur if a user taps a button. If a text label is not used, an icon should be present to signify what the button does. By default Material uses capitalizedbutton text labels (for languages that have capitalization). This is to distinguish the text label from surrounding text. If a text button does not use capitalization for button text, find another characteristic to distinguish it such as color,size, or placement." },
        ];
    }
    ActionsheetSimplePage.prototype.ngOnInit = function () {
    };
    //action sheet controller function
    ActionsheetSimplePage.prototype.presentActionSheet = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var actionSheet;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.actionSheetController.create({
                            header: 'Albums',
                            buttons: [{
                                    text: 'Delete',
                                    role: 'destructive',
                                    handler: function () {
                                        console.log('Delete clicked');
                                    }
                                }, {
                                    text: 'Share',
                                    handler: function () {
                                        console.log('Share clicked');
                                    }
                                }, {
                                    text: 'Play (open modal)',
                                    handler: function () {
                                        console.log('Play clicked');
                                    }
                                }, {
                                    text: 'Favorite',
                                    handler: function () {
                                        console.log('Favorite clicked');
                                    }
                                }, {
                                    text: 'Cancel',
                                    role: 'cancel',
                                    handler: function () {
                                        console.log('Cancel clicked');
                                    }
                                }]
                        })];
                    case 1:
                        actionSheet = _a.sent();
                        return [4 /*yield*/, actionSheet.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ActionsheetSimplePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-actionsheet-simple',
            template: __webpack_require__(/*! ./actionsheet-simple.page.html */ "./src/app/actionsheet-simple/actionsheet-simple.page.html"),
            styles: [__webpack_require__(/*! ./actionsheet-simple.page.scss */ "./src/app/actionsheet-simple/actionsheet-simple.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"]])
    ], ActionsheetSimplePage);
    return ActionsheetSimplePage;
}());



/***/ })

}]);
//# sourceMappingURL=actionsheet-simple-actionsheet-simple-module.js.map