(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["parallax-header-image-parallax-header-image-module"],{

/***/ "./src/app/parallax-header-image/parallax-header-image.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/parallax-header-image/parallax-header-image.module.ts ***!
  \***********************************************************************/
/*! exports provided: ParallaxHeaderImagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParallaxHeaderImagePageModule", function() { return ParallaxHeaderImagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _parallax_header_image_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./parallax-header-image.page */ "./src/app/parallax-header-image/parallax-header-image.page.ts");







var routes = [
    {
        path: '',
        component: _parallax_header_image_page__WEBPACK_IMPORTED_MODULE_6__["ParallaxHeaderImagePage"]
    }
];
var ParallaxHeaderImagePageModule = /** @class */ (function () {
    function ParallaxHeaderImagePageModule() {
    }
    ParallaxHeaderImagePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_parallax_header_image_page__WEBPACK_IMPORTED_MODULE_6__["ParallaxHeaderImagePage"]]
        })
    ], ParallaxHeaderImagePageModule);
    return ParallaxHeaderImagePageModule;
}());



/***/ }),

/***/ "./src/app/parallax-header-image/parallax-header-image.page.html":
/*!***********************************************************************!*\
  !*** ./src/app/parallax-header-image/parallax-header-image.page.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>parallax-header-image</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/parallax-header-image/parallax-header-image.page.scss":
/*!***********************************************************************!*\
  !*** ./src/app/parallax-header-image/parallax-header-image.page.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhcmFsbGF4LWhlYWRlci1pbWFnZS9wYXJhbGxheC1oZWFkZXItaW1hZ2UucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/parallax-header-image/parallax-header-image.page.ts":
/*!*********************************************************************!*\
  !*** ./src/app/parallax-header-image/parallax-header-image.page.ts ***!
  \*********************************************************************/
/*! exports provided: ParallaxHeaderImagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParallaxHeaderImagePage", function() { return ParallaxHeaderImagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ParallaxHeaderImagePage = /** @class */ (function () {
    function ParallaxHeaderImagePage() {
    }
    ParallaxHeaderImagePage.prototype.ngOnInit = function () {
    };
    ParallaxHeaderImagePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-parallax-header-image',
            template: __webpack_require__(/*! ./parallax-header-image.page.html */ "./src/app/parallax-header-image/parallax-header-image.page.html"),
            styles: [__webpack_require__(/*! ./parallax-header-image.page.scss */ "./src/app/parallax-header-image/parallax-header-image.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ParallaxHeaderImagePage);
    return ParallaxHeaderImagePage;
}());



/***/ })

}]);
//# sourceMappingURL=parallax-header-image-parallax-header-image-module.js.map